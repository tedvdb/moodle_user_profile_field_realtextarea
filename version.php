<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2011112900;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2011112900;        // Requires this Moodle version
$plugin->component = 'profilefield_realtextarea'; // Full name of the plugin (used for diagnostics)
$plugin->maturity = MATURITY_RC;
$plugin->release = $plugin->release = '2.x (Build: 2011112900)'; 